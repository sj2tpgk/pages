#+title: Planet without laughters - Smullyan
#+options: toc:t
#+html_head: <!-- [[title=Planet without laughters - Smullyan]] [[mtime=2022-07-02]] [[desc=ユーモアのない惑星???]] -->
#+html_head: <meta http-equiv="Content-Security-Policy" content="default-src * data: blob: 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval';"> <!-- 'self' not self -->
#+BEGIN_EXPORT html
<style>
#table-of-contents > h2 { display: none }
body { margin: 0; background: url("./smullyan_planet_without_laughters/img-bg.png"); line-height: 1.7 }
header, nav    { max-width: 750px; margin: 2em auto }
.outline-2     { width: 100%; max-width: 100%; margin: 11em 0; padding: 3em 0; box-shadow: #ddd 0 0 15px 0px }
.outline-2 > * {
    width: 750px; margin: 3em auto;
    background: rgba(255, 255, 255, 50%);
    box-shadow: #ccc 0px 0px 5px 2px;
    padding: 1em .5em;
}
.outline-2 > h2 { width: fit-content; }
html * { background-repeat: repeat }

.outline-2:nth-of-type(1) { background-image: url("./smullyan_planet_without_laughters/img-1.png"); margin-top: 2em }
.outline-2:nth-of-type(1) > * { border-radius: 0em; box-shadow: #ddd 0px 0px 4px 1px; border: solid blue 1px }

.outline-2:nth-of-type(2) { background-image: url("./smullyan_planet_without_laughters/img-i.png") }
.outline-2:nth-of-type(2) > * { border-radius: 0; background: rgba(255, 255, 255, 65%);  }

.outline-2:nth-of-type(3) { background-image: url("./smullyan_planet_without_laughters/img-ii.png") }
.outline-2:nth-of-type(3) > * { border-radius: 2em; background: rgba(255, 255, 255, 75%); box-shadow: #ff9 0px 0px 5px 2px; }

.outline-2:nth-of-type(4) { background: radial-gradient(circle at 50% 20%, rgba(255, 255, 90, 50%) 20%, rgba(255, 255, 90, 10%) 60%), linear-gradient(to bottom, #fdd, #bdffb4 50%) }
.outline-2:nth-of-type(4) > * { border-radius: 2em; box-shadow: #aff 0px 0px 5px 2px; }

.outline-2:nth-of-type(5) { background-image: url("./smullyan_planet_without_laughters/img-v.png") }
.outline-2:nth-of-type(5) > * { border-radius: 1em }

.outline-2:nth-of-type(6) { background-image: url("./smullyan_planet_without_laughters/img-vi.png") }
.outline-2:nth-of-type(6) > * { border-radius: 2em; background: rgba(255, 255, 255, 75%); box-shadow: #fcc 0px 0px 5px 2px; }

.outline-2:nth-of-type(7) { background-image: none; margin-top: 0; background: rgb(252, 232, 227); margin-bottom: 0 }
.outline-2:nth-of-type(7) > * { border-radius: 0em; box-shadow: #eee 0px 0px 10px 2px; padding-left: 1em; padding-right: 1em }
.outline-2:nth-of-type(7) > h2 { border: ridge red 2px }
.outline-2:nth-of-type(7) > .outline-text-2 { display: none }


@media screen and (max-aspect-ratio: .75) and (max-width: 13cm) { /* Mobile */
    body { width: 100% }
    header, nav { max-width: 100% }
    .outline-2 { width: 100% }
    .outline-2 > * { width: unset; padding: 1em 1em }
    * { word-break: break-all }
    .org-ul { padding-left: 1em }
}
</style>
#+END_EXPORT

** まえがき

Raymond Smullyan 作の [[https://www-cs-faculty.stanford.edu/~knuth/smullyan.html][Planet Without Laughter]] の要約

*Smullyan について*

Smullyan は多くの著作を残しており、
私もその中の一つ "To Mock a Mockingbird" を読んだことがあった。
その本では
「全ての計算は S,K,I という3つの演算子から構成できる」という理論などを、
演算子を鳥で、その作用を鳴き声による会話で例えて伝えており、
大変興味深いと感じた。

今回の短編は、Knuth の「コンピュータ科学者がめったに話さないこと」という本で、
Knuth が講演中に紹介していたものである。

以下、意訳・詳細の省略あり

** I. The Modern Period
- ある惑星の住人はたいへんまじめであった。
- しかし *laugher* と呼ばれる一部の住人は笑ったり冗談を言ったりした。
- 他の住人からは、その振舞いは変なものと見られていた。
- laugher は他の人に伝染してしまうため、惑星では laugher はなくすべきだと考えられるようになった。
- laugher を "一時的に" 治療する (ように見える) 薬ができた。
- しかし「薬の副作用が苦しいので、laughter は治癒されたふりをしており、さらなる薬の投与を免れようとしているのではないか??」という疑念が生まれた。
- つまり laughter は不誠実なのではないか?? これは、けしからん!

** II. The Middle Period, III. A Sermon
- この惑星の歴史は、Ancient, Middle, Modern に分けられる。
- かつてこの惑星は、笑い、冗談、ユーモアで満ちあふれていた。 (Ancient 時代)
- Middle になるとその退潮が始まった。
  + 人々はその退潮に危機感を持った。
  + ユーモアを持たないものは、いろいろな手段で獲得を試みた。
  + Laugher への弟子入りを試みる人、ひたすら Laughter の真似をする人、信仰によりユーモアを得ようとする人など。
  + Laugher から見れば、ユーモアを「意図的に」「習得する」という考え自体、おもしろおかしなことであった。
- やがてユーモアへの信仰運動が主流となり、各地に教会ができた。
- ユーモアの退潮は止まらず、その理由について、次のような伝説が語られるようになった。

** IV. The Great Legend
- 昔、アダムとイブは神とともに、ユーモアに満ちた世界で平和に暮らしていた。
- 緑色の動物がやってきて、アダムとイブに、二人が *自由意志* を持っていることを伝え、神との決別をそそのかした。
- 二人はその提案を一度は拒否するが、次第に神を信じられなくなっていった。
- そしてある日、二人は神と決別した。
- 神による新たなユーモアの供給が途絶え、世界から笑いが消えていくことになった。

** V. Back to the Modern Period
- Modern Period になると、ユーモアの衰退はむしろ歓迎すべきことだと考えられるようになった。
- Laugher は隔離され、永遠に薬を投与され続ける laugh-scream 病院か、治療を諦めて何もしない pure-laugh 病院へと入れられていった。
- Scream 病院の患者は、次々と死亡した。
- 一方、pure 病院では、隔離されている点を除けば、非常に良い暮らしを送っていた。
- そのため、外の世界にいる laugher や、さらには non-laugher にさえも、pure 病院へと向かうものが現れた。
- Pure 病院に入り込んだ non-laugher は、感化されて laugher になった。
- そうして、scream 病院が減る一方、pure 病院には人が溢れ、pure-laugh の町や国が作られた。
- なんと、最後には全ての住人が laugher となった。

# えらいことになった

** VI. Epilogue in Heaven 
- この歴史は、全て神の計画であったことが明かされる。
- 神は、何もしなければ世界から笑いが消えることを予知していた。
- アダムとイブに行動を起こさせるために、二人が自由意志を持っていることを自覚させる必要があった。というのも、彼らは自尊心が高いので、 /自ら/ が決めた(と感じた)ことでないと、行動を起こさないからである。
- 神は、緑の生物 Nemod にその役を任せることにした。つまり、まず Nemod に自由意志を持っていることを自覚させる必要があった。
- そのために、神は自分自身に、自由意志を自覚させる必要があった。
- これを自己暗示によって実現させた。
- そこから先は、からくりのように歴史が展開された。


** あとがき

*** launghter, humor の訳
laughter を直訳すると「笑い」だが、日本語で名詞として使いすぎると不自然

笑い声、笑顔をもたらす 等の言い換えを使うと良い (爆笑の渦、笑う門といったことばもあり)

laughter に1対1に対応する単語がない!?

# 笑顔と訳すのは変
# (理由 反対は 笑顔がない となり、それは不幸といった意味合いを感じるが、 non-laughter を不幸というのは違和感あり)

laughter 心のゆとり 乱雑さ 非合理
↔
整然 真面目 合理的 計画的

humor の訳
「笑い」「ユーモア」「とんち」「機知」などがある

*** ユーモアの復活
Modern Period での、 non-laugher が自ら病院に入り、感化されて laugher になってしまうところに、
ドタバタ・何でもあり感がある

*** 自由意志
まだ理解できていない点がある。

最後の Epilogue in Heaven では、アダムとイブの自由意志は緑の動物によって感化されたものであり、
その緑の生物の自由意志は神によって感化されたものであり、
さらに神の自由意志は自己暗示によって一時的に得られたものであるということが明かされていた。

しかし、その自己暗示自体、自由意志がなければできないものなのではないか、という疑問が残った。
一時的に自由意志を持つという計画を立て実行したが、
その計画を立てること自体、自由意志が必要なのではないか。

また、自由意志はユーモアと対立しているようだと途中の時点では思っていたが、そうではないようだった。


# 自由意志と話の流れの関わりが今ひとつつかめていない。

# 絵を書こう
# 惑星の図 四角がたくさん 丸が少し
# 楽園 色だけ 人物などなし
# 
# 文章の背景もこだわる
# 
# laughter視点で
# 
# 惑星が話す形式 セリフ形式
# 
# 丸から四角へ、
# そして
# 四角が吸い込まれて、銀河系のような図、再び多数の曲線になる。
