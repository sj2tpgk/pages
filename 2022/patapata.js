const font = font5x5;

const shapeConstructorNames = [ "G", "Rect" ];

function MyCanvas(elem) {

    // Private fields
    const T = this;
    const minTime = 40;
    let time = 0;
    let invalid = true;
    let wasResized = false;

    // Public fields
    T.ctx = elem.getContext("2d");
    T.w = 0; T.h = 0;
    T.clear = () => T.ctx.clearRect(0, 0, T.w, T.h);
    T.onresize = null;
    T.theG = new MyCanvas.G(0, 0);
    T.draw = () => { T.theG.draw(T) }
    T.invalidate = () => { invalid = true; window.requestAnimationFrame(anim); }

    // define shape adders e.g.
    // T.rect = (...args) => T.theG.rect(...args)
    for (let shapeConstructorName of shapeConstructorNames) {
        const shapeFuncName = shapeConstructorName.toLowerCase();
        T[shapeFuncName] = (...args) => {
            return T.theG[shapeFuncName](...args);
        }
    }

    // Private funcs
    function resize() {
        // Setting elem.width and elem.height here will erase canvas and cause
        // flickering. We must postpone it until next requestAnimationFrame.
        T.w = document.documentElement.clientWidth;
        T.h = document.documentElement.clientHeight;
        wasResized = true;
        T.invalidate();
    }
    function anim(time2) {
        if (invalid && time2 - time > minTime) {
            if (wasResized) {
                elem.width  = T.w;
                elem.height = T.h;
                wasResized = false;
            }
            T.clear();
            T.draw();
            time = time2;
            invalid = false;
        }
        if (invalid) window.requestAnimationFrame(anim);
    }

    // Init
    resize();
    window.addEventListener("resize", () => {
        resize(); resize();
        if (T.onresize) T.onresize();
    });
    elem.addEventListener("click", ev => {
        const x = ev.clientX, y = ev.clientY;
        T.theG.fireclick(x, y);
    });
    window.requestAnimationFrame(anim);
}

MyCanvas.G = function(x, y) {

    // Private fields
    const T = this;
    const objs = [];

    // Public fields
    T.x = x; T.y = y;

    // define shape adders e.g.
    // T.rect = (x, y) => { add a rect to T; }
    for (let shapeConstructorName of shapeConstructorNames) {
        const shapeConstructor = MyCanvas[shapeConstructorName];
        const shapeFuncName = shapeConstructorName.toLowerCase();
        T[shapeFuncName] = (...args) => {
            const shape = new shapeConstructor(...args);
            objs.push(shape);
            return shape;
        }
    }

    // Try firing onclick; named "fireclick" to make "onclick" prop configurable
    T.fireclick = (x, y) => {
        for (let o of objs)
            if (o.fireclick(x-T.x, y-T.y))
                return true;
        if (T.onclick) console.error("Click event on MyCanvas.G is not implemented");
        return false;
    }

    T.getZindex = () => 0;

    T.draw = c => {
        c.ctx.translate(T.x, T.y);
        let zindices = [], seen = {};
        for (let o of objs) {
            if (!seen[o.getZindex()]) {
                zindices.push(o.getZindex());
                seen[o.getZindex()] = true;
            }
        }
        zindices.sort();
        for (let z of zindices)
            for (let o of objs)
                if (o.getZindex() == z)
                    o.draw(c);
        c.ctx.translate(-T.x, -T.y);
    }

    return T;
}

MyCanvas.Rect = function(x, y, w, h) {

    const T = this;
    let color, stroke, lineWidth, zindex = 0, alpha;
    T.color       = val => { color       = val; return T }
    T.alpha       = val => { alpha       = val; return T }
    T.stroke      = val => { stroke      = val; return T }
    T.lineWidth   = val => { lineWidth   = val; return T }
    T.zindex      = val => { zindex      = val; return T }
    T.getZindex = () => zindex;
    T.x = x; T.y = y; T.w = w; T.h = h;

    T.draw = c => {
        let asav = c.ctx.globalAlpha;
        if (alpha) c.ctx.globalAlpha = alpha;
        let swsav = c.ctx.lineWidth;
        if (lineWidth) c.ctx.lineWidth = lineWidth;
        if (stroke) {
            if (color) c.ctx.strokeStyle = color;
            c.ctx.strokeRect(T.x, T.y, T.w, T.h);
        } else {
            if (color) c.ctx.fillStyle = color;
            c.ctx.fillRect(T.x, T.y, T.w, T.h);
        }
        if (lineWidth) c.ctx.lineWidth = swsav;
        if (alpha) c.ctx.globalAlpha = asav;
    }

    T.fireclick = (x, y) => {
        if (T.onclick && inside(x, y)) { T.onclick(); return true }
    }

    T.onclick = null;

    // Private funcs
    var inside = (x, y) => (T.x <= x && x <= T.x + T.w) && (T.y <= y && y <= T.y + T.h);

    // Chain
    return T;
}

function Panel(c, g, size, ix, iy) {
    const x0 = size*ix, y0 = size*iy, h0 = size*.8, w0 = size*.8;
    const rect = g.rect(x0, y0, h0, w0).color(color(0));
    let timer;
    this.value = false;
    rect.onclick = () => {
        this.flip();
    }
    this.flip = (horiz) => {
        if (timer) clearInterval(timer);
        const n = 20;
        let i = 0;
        const sgn = this.value ? 1 : -1;
        this.value = !this.value;
        rect.x = x0; rect.y = y0; rect.w = w0; rect.h = h0;
        timer = setInterval(() => {
            const t = 1 - (i/n) * 2; // -1 to 1
            const th = Math.PI * .5 * (t>=0?1:-1) * Math.pow(Math.abs(t), .5);
            const h = Math.abs((h0/2) * Math.sin(th));
            if (horiz) {
                rect.x = x0 + (h0/2) - h;
                rect.w = 2 * h;
            } else {
                rect.y = y0 + (h0/2) - h;
                rect.h = 2 * h;
            }
            rect.color(color((sgn * Math.sin(th) / 2) + .5));
            c.invalidate();
            if (++i > n) { clearInterval(timer); timer = null }
        }, 15);
    }
    function color(val) {
        const v = val < .5 ? ((.5 - val) * 2 * 255 * .2) : ((val - .5) * 2 * 255 * .8);
        return `rgb(${v},${v},${v})`;
    }
}

window.onload = function() {
    const c = new MyCanvas(Q("canvas"));

    // settings
    const rows = cols = 32;
    let size = Math.floor(Math.min(c.w, c.h) / 32);
    let panelsX0 = Math.floor((c.w - size*32) / 2), panelsY0 = 8;

    const g = c.g(panelsX0, panelsY0);

    const panels = Array2D(cols, rows, (i, j) => new Panel(c, g, size, i, j));
    const get = (x, y) => panels[x][y].value;
    const set = (x, y, v) => { if (v ^ get(x, y)) panels[x][y].flip() }
    initWheels(panels, rows, cols);


    function initWheels() {
        // Settings
        const n = 32;
        const base = 8;
        const fontSize = 5;

        // Array of 0s and 1s of length 32
        const wheelData = "00000101011 0010011011 1110 001110 1".replace(/ /g, "").split("").map(ch => ch == "1");
        for (let ix=0; ix<n; ix++)
            for (let iy=0; iy<n; iy++)
                set(ix, iy, wheelData[iy]);

        // States
        const currentSpin = [...Array(n)].map(_ => 0);
        let msg = "Hello";

        // Resizing
        myResized();
        c.onresize = myResized;
        function myResized() {
            size = Math.floor(Math.min(c.w, c.h) / n);
            g.x = panelsX0 = Math.floor((c.w - size*n) / 2);
        }

        // Overlay: blinds
        [
            g.rect(0, 0, n*size, base*size),
            g.rect(0, (base+fontSize)*size, n*size, (n-base)*size)
        ].forEach(rect => rect.color("rgb(0,0,0)").stroke(false).zindex(1).alpha(.6));

        // Overlay: frame
        const pad = size*.2;
        g.rect(-pad, base*size-pad, n*size+pad, fontSize*size+pad)
            .color("#4ff").stroke(true).lineWidth(4).zindex(1);

        // First run after a short delay
        setTimeout(update, 500);

        // Append to message on keydown
        window.onkeydown = ({key}) => {
            if (font[key]) {
                msg = msg.substr(1, msg.length - 1) + key;
                update();
            }
        }

        function update() {
            for (let i=0; i<msg.length; i++) {
                const ch = msg[i];
                const ix0 = i*(fontSize+1);
                spinTo(fontSize+ix0, getSpin([0, 0, 0, 0, 0]));
                for (let ix=ix0; ix<ix0+fontSize; ix++) {
                    const spin = getSpin(font[ch][ix-ix0]);
                    spinTo(ix, spin);
                }
            }
            for (let ix=(fontSize+1)*msg.length; ix<n; ix++)
                spinTo(ix, getSpin([0, 0, 0, 0, 0]));
        }

        function getSpin(data5) {
            loop: for (let s=0; s<n; s++) {
                for (let i=0; i<fontSize; i++) {
                    if (wheelData[mod(base-s+i, n)] != data5[i])
                        continue loop;
                }
                return s;
            }
        }

        function spinTo(ix, spin) {
            const timer = setInterval(() => {
                if (currentSpin[ix] == spin) { clearInterval(timer); return; }

                const willFlip = [];
                for (let iy=0; iy<n; iy++)
                    willFlip[iy] = (panels[ix][iy].value ^ panels[ix][mod(iy-1, n)].value);

                for (let iy=0; iy<n; iy++)
                    if (willFlip[iy])
                        panels[ix][iy].flip();

                currentSpin[ix] = mod(currentSpin[ix]+1, n);
            }, 150);
        }
    }
}


function mod(x, y) { return ((x%y)+y)%y; }

function Array2D(n1, n2, init) {
    const a = [];
    const isFunc = typeof init == "function";
    for (let i=0; i<n1; i++) {
        a[i] = [];
        for (let j=0; j<n2; j++)
            a[i][j] = isFunc ? init(i, j) : init;
    }
    return a;
}
