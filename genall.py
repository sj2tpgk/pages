#!/usr/bin/env python

# TODO refactor

import json
import os
import re
import subprocess

def parseMeta(s):
    return { k: v for (k, v) in re.findall(r"\[\[([^=]*)=([^]]*)", s) }

def shellSplit(shScript):
    output = subprocess.check_output(shScript, shell=True)
    return output.decode().splitlines()

def writeFile(file, s):
    f = open(file, "w")
    f.write(s)
    f.close()

def get(dic, prop):
    return prop in dic and dic[prop]

def getEmoji(path):
    ext = str.lower(os.path.splitext(path)[1])
    if ext in ['.svg', '.png']:
        return '🖼️'
    elif ext in ['.org']:
        return '📄'
    elif path.endswith('/'):
        return '📁'
    else:
        return '📄'

def genHTML(d):
    depth = len(re.findall(r"/", d))
    out = """<!doctype html>
<html lang="ja">
<head>
  <title>ALL POSTS - my page</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>

body { max-width: max-content; margin: 0 auto; padding: 0 }

a:hover { background: blue; color: white }

thead     { text-align: center; text-decoration: underline }
tr.info2  { display: none }
td        { padding-right: .5em }
td.type   { width: 1.5em; text-align: left }
td.name a { width: 16em; overflow-x: hidden; white-space: nowrap; text-overflow: ellipsis; display: block }
td.tags   { text-align: center }
span.desc { padding-left: 2em; color: #555; width: 18em; font-size: small; overflow-x: hidden; white-space: nowrap; text-overflow: ellipsis; display: block }

ul, li    { padding-left: 0; white-space: nowrap; list-style: none; line-height: 1.5 }
ul#top ul { padding-left: 2em }

@media screen and (max-aspect-ratio: .75) and (max-width: 13cm) { /* Mobile */
    body { max-width: 100%; margin: 0 1em }
    h3   { text-align: center }
    thead             { display: none }
    td.name a         { white-space: normal; word-break: break-all; display: inline; }
    td.mtime, td.tags { display: none }
    tr.info1 span     { padding-left: 1.5em; white-space: normal; word-break: break-all; width: 100% }
    tr.info2          { display: table-row }
    tr.info2 td.text  { padding-left: 1.5em; color: #555; font-size: small }
}

  </style>
</head>
<body class="wp-admin">
  <h3>ALL POSTS</h3>
  <table>
    <thead><tr><td class="type"></td><td class="name">名前</td><td class="tags">タグ</td><td class="mtime">最終更新</td></tr></thead>
    <tbody>
"""

    for f in files:
        emoji = getEmoji(f["path"])
        href = ("../" * depth) + f["path"]
        title = get(f, "title") or os.path.basename(f["path"])
        mtime = get(f, "mtime") or get(f, "btime") or "?"
        tags = ",".join(sorted(re.split(r",\s*", get(f, "tags") or "")))
        desc = get(f, 'desc') or ""
        out += f"""      <tr>
        <td class="type">{emoji}</td>
        <td class="name"><a href="{href}" target="_parent">{title}</a></td>
        <td class="tags">{tags}</td>
        <td class="mtime">{mtime}</td>
      </tr>
      <tr class="info1"><td></td><td><span class="desc">{desc}</span></td></tr>
      <tr class="info2"><td></td><td class="text">{mtime} {tags}</td></tr>
"""
    out += """    </tbody>
  </table>
  <h3>ALL FILES</h3>
"""

    def treeToHTML(t, nest, path):
        ind = "  " * (nest + 2)
        if type(t) is list:
            return "".join([treeToHTML(tt, nest, path) for tt in t])
        elif t["type"] == "file":
            path2 = path + '/' + t['name']
            return ind + f"""<li>{getEmoji(path2)} <a href="{path2}" target="_parent">{t["name"]}</a></li>\n"""
        elif t["type"] == "directory":
            sub = treeToHTML(t["contents"], nest + 1, path + "/" + t["name"]) if "contents" in t else ""
            return ind + "<li>" + getEmoji("/") + ' ' + t["name"] + "</li>\n" + ind + "<ul>\n" + sub + ind + "</ul>\n"
    out += "  <ul id=\"top\">\n" + treeToHTML(tree["contents"], 0, ("../" * depth) + ".") + "  </ul>\n"

    out += """  </body>
</html>
"""
    return out


## main

tree = json.loads("".join(shellSplit("tree -I .git -I _files.html -J")))[0]

files = []
shScript = """find . -type f | sed -n '/_files.html/d; /html$/p; /svg$/p;' | while read f; do echo -n "$f"; echo -n "===="; sed -n '/\\[\\[/{p;q};7q' "$f"; echo; done | sed '/^$/d'"""
for l in shellSplit(shScript):
    [path, meta] = l.split("====")
    dic1 = parseMeta(meta)
    dic1["path"] = path
    files += [dic1]
files.sort(reverse=True, key=lambda dic: get(dic, "mtime") or get(dic, "btime") or "0000-00-00")

for d in shellSplit("""find . -type d | sed '/.git/d'"""):
    writeFile(d + "/_files.html", genHTML(d))
