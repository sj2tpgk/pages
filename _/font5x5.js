font5x5 = (function() {
    const data =
`
-----
A
 xxx
x   x
xxxxx
x   x
x   x

-----
B
xxxx
x   x
xxxx
x   x
xxxx

-----
C
 xxx
x
x
x
 xxx

-----
D
xxxx
x   x
x   x
x   x
xxxx

-----
E
xxxxx
x
xxxxx
x
xxxxx

-----
F
xxxxx
x
xxxx
x
x

-----
G
 xxx
x
x  xx
x   x
 xxx

-----
H
x   x
x   x
xxxxx
x   x
x   x


-----
I
 xxx
  x
  x
  x
 xxx

-----
J
 xxxx
   x
   x
x  x
 xx

-----
K
x   x
x  x
xxx
x  x
x   x

-----
L
x
x
x
x
xxxxx

-----
M
xx xx
x x x
x x x
x x x
x x x

-----
N
x   x
xx  x
x x x
x  xx
x   x

-----
O
 xxx
x   x
x   x
x   x
 xxx

-----
P
xxxx
x   x
xxxx
x
x

-----
Q
 xxx
x   x
x   x
x x x
 xxxx

-----
R
xxxx
x   x
xxxx
x  x
x   x

-----
S
 xxxx
x
 xxx
    x
xxxx

-----
T
xxxxx
  x
  x
  x
  x

-----
U
x   x
x   x
x   x
x   x
 xxx

-----
V
x   x
x   x
x   x
 x x
  x

-----
W
x x x
x x x
x x x
x x x
 x x

-----
X
x   x
 x x
  x
 x x
x   x

-----
Y
x   x
 x x
  x
  x
  x

-----
Z
xxxxx
   x
  x
 x
xxxxx



-----
a
 xxx
    x
 xxxx
x   x
 xxxx

-----
b
x
x
xxxx
x   x
xxxx

-----
c
 xxx
x   x
x
x   x
 xxx

-----
d
    x
    x
 xxxx
x   x
 xxxx

-----
e
 xxx
x   x
xxxxx
x
 xxx

-----
f
   xx
  x
xxxxx
  x
  x

-----
g
 xxxx
x   x
 xxxx
    x
 xxx

-----
h
x
x
xxxx
x   x
x   x

-----
i
  x

 xx
  x
 xxx

-----
j
   x

  xx
   x
 xx

-----
k
x
x x
xx
x x
x  x

-----
l
 xx
  x
  x
  x
 xxx

-----
m
 x x
x x x
x x x
x x x
x x x

-----
n
x xx
xx  x
x   x
x   x
x   x

-----
o

 xxx
x   x
x   x
 xxx

-----
p
x xx
xx  x
x xx
x
x

-----
q
 xx x
x  xx
 xx x
    x
    x

-----
r
x xx
xx  x
x
x
x

-----
s
 xxx
x
 xxx
    x
 xxx

-----
t
  x
xxxxx
  x
  x
   xx

-----
u
x   x
x   x
x   x
x  xx
 xx x

-----
v

x   x
x   x
 x x
  x

-----
w

x   x
x   x
x x x
 x x

-----
x

xx  x
  xx
 xx
x  xx

-----
y
x   x
x   x
 xxxx
    x
 xxx

-----
z

xxxxx
   x
 xx
xxxxx

`;

    const size = 5;
    const lines = data.split("\n");
    const obj = {};
    let i = 0;
    while (i < lines.length) {
        if (lines[i].startsWith("-----")) {
            const ch = lines[i+1][0];
            obj[ch] = [];
            for (let x=0; x<size; x++) {
                obj[ch][x] = [];
                for (let y=0; y<size; y++)
                    obj[ch][x][y] = lines[i+2+y][x] == "x";
            }
            i += 7;
        } else {
            i++;
        }
    }

    return obj;
})();
