// season art (fruit, woods, birds, snow)
const arts = [ ArtRainbow, ArtErathos ];

function ArtRainbow() {
    // let timerID = false;
    const msgs = ["Hello", "Welcome", "(o-o)"];
    const msg = msgs[rnd(msgs.length)];
    let i = 0;
    this.init = function(c) {
        const toggle = toggleInterval(() => draw(c, i+=.1), 100, false);
        c.elem.addEventListener("click", toggle);
    }
    this.resized = function(c) { draw(c, i) }
    function draw(c, i) {
        const s = 9;
        const hue = (x, y) => x + (1+i*.00)*y + i*3;
        for (let x = 0; x < c.w; x += s)
            for (let y = 0; y < c.h; y += s) {
                c.ctx.fillStyle = `hsl(${hue(x,y)}deg,80%,80%)`;
                c.ctx.fillRect(x, y, s, s);
            }
        const ss = 14;
        const bx = c.w/2 - msg.length*6*ss*.5;
        const by = c.h/2 - 5*ss*.5;
        for (let i = 0; i < msg.length; i++) {
            const ch = msg.charAt(i);
            if (!ch) { c.ctx.fillStyle = `#000`; c.ctx.fillRect(0, 0, 30, 30); return } // error
            font[ch].split(",").forEach((line, row) => {
                line.split("").forEach((val, col) => {
                    if (val != " ") {
                        const x = bx + col*ss + i*6*ss;
                        const y = by + row*ss;
                        c.ctx.fillStyle = `hsl(${hue(x+ss/2,y+ss/2)}deg,90%,70%)`;
                        c.ctx.fillRect(x, y, ss, ss);
                    }
                });
            });
        }
    }
}

function ArtErathos() {
    this.init = function(c) {
        const toggle = toggleInterval(() => draw(c, ++i), 100, false);
        c.elem.addEventListener("click", toggle);
    }
    this.resized = function(c) { draw(c, i) }
    const maxn = 1000;
    const s = 10;
    const a = [];
    function draw(c, i) {

    }
    function* gen(maxn, a) {
        for (let i=0; i<=maxn; i++) a[i] = true;
        a[0] = a[1] = false;
        let d = 3;

        yield true;
    }
}

const font = { // 5x5 bitmap font
    H  : "x   x,x   x,xxxxx,x   x,x   x",
    W  : "x   x,x   x,x x x,x x x, x x ",
    Y  : "x   x, x x ,  x  ,  x  ,  x  ",
    c  : " xxxx,x    ,x    ,x    , xxxx",
    e  : " xxx ,x   x,xxxxx,x    , xxx ",
    l  : " xx  ,  x  ,  x  ,  x  , xxx ",
    m  : " x x ,x x x,x x x,x x x,x x x",
    o  : " xxx ,x   x,x   x,x   x, xxx ",
    "-": "     ,     , xxx ,     ,     ",
    "(": "  x  , x   , x   , x   ,  x  ",
    ")": "  x  ,   x ,   x ,   x ,  x  ",
}

function hsl2rgb(h, s, l) {
    // h in [0,360], s and l in [0,100]
    // return [r,g,b], each in [0,255]
    h /= 360;
    s /= 100;
    l /= 100;

    var r, g, b;

    if (s == 0) {
        r = g = b = l; // achromatic
    } else {
        function hue2rgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1/6) return p + (q - p) * 6 * t;
            if (t < 1/2) return q;
            if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;

        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [ Math.floor(r * 255), Math.floor(g * 255), Math.floor(b * 255) ];
}

function Array2D(n1, n2, init) {
    const a = [];
    for (let i=0; i<n1; i++) {
        a[i] = [];
        for (let j=0; j<n2; j++) a[i][j] = init;
    }
    return a;
}

function toggleInterval(func, interval, startNow) {
    let timer = false;
    if (startNow) toggle();
    return toggle;
    function toggle() {
        if (timer) {
            clearInterval(timer);
            timer = false;
        } else {
            timer = setInterval(func, interval);
        }
    }
}

window.onload = function() {
    const art = new arts[rnd(arts.length)];

    const cvs = Q("#art");
    const c = { elem: cvs, ctx: cvs.getContext("2d"), w: 0, h: 0 };
    art.init(c);
    resized();
    window.onresize = resized;

    function resized() {
        // let bounding = cvs.getBoundingClientRect();
        // cvs.width  = c.w = parseInt(bounding.width);
        // cvs.height = c.h = parseInt(bounding.height);
        cvs.width  = c.w = document.documentElement.clientWidth;
        cvs.height = c.h = document.documentElement.clientHeight;
        art.resized(c);
    }
}


